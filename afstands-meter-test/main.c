#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */

    //Zet display aan
    oledInitialize();
    //eventueel flippen
    oledSetOrientation(FLIPPED);
    //begin met leeg scherm
    oledClearScreen();

    oledPrint(20,1, "afstand", small);
    uint8_t kolom,n=0;

    int b = 0;

    while(1){
        int a;
        char afstnd[10];
        a=3;
        b=a*40;
        itoa(b, afstnd);
        oledPrint(20,4, afstnd , small);

        //return 0;


    }
}
